defmodule LixEval do
  use LixEval.Types

  @moduledoc ~S"""

    # Abstract

    Lix is a simple Lisp dialect which is defined in the  [`LixParser`](https://hex.pm/packages/lix_eval)` into an AST.

    This _Evaluator_ defines some _Special Forms_ and evaluates a Lix ast or source code for a given environnment.
    A default environment (a core runtime if yu want) is also defined, but can be overridden by any client application.


    The following doctests describe the _semantics_ of Lix


    ## Evaluation with an empty environnement

    ### Literal values

    If we need to evaluate a source we will call `parse_eval`

        iex(1)> parse_eval("42")
        {:ok, {%{}, 42}}

    In case we already have an AST, we can call `evaluate`

        iex(2)> evaluate({:lit, 43})
        {:ok, {%{}, 43}}

    If the result only is needed we can use the bang versions

        iex(3)> parse_eval!(":hello")
        :hello

        iex(4)> evaluate!({:lit, "string"})
        "string"
  
    ### Identifiers

    Apart from special forms, identifiers are looked up inside the environnement

        iex(5)> parse_eval!("a", %{"a" => 44})
        44

    They can be composed, which implements simple name spaces

        iex(6)> parse_eval!("a.b", %{"a" => %{"b" => 45}})
        45

    And undefined values are reported as errors

        iex(7)> parse_eval("a.c.d", %{"a" => %{"b" => 45}})
        {:error, "Undefined name c (a.c.d) at {1, 1}"}

    ... and yes, error messages are still bad.

  """

  @spec evaluate(LixParser.ast_t, map()) :: result_t() 
  defdelegate evaluate(ast, env \\ %{}), to: __MODULE__.Evaluator

  @spec evaluate!(LixParser.ast_t, map()) :: any()
  def evaluate!(ast, env \\ %{}) do
    # NO with here
    {:ok, {_env, result}} = evaluate(ast, env)
    result
  end

  @spec parse_eval(binary(), map()) :: result_t()
  defdelegate parse_eval(source, env \\ %{}), to: __MODULE__.Evaluator

  @spec parse_eval!(binary(), map()) :: any()
  def parse_eval!(source, env \\ %{}) do
    # NO with here
    {:ok, {_env, result}} = parse_eval(source, env)
    result
  end

end
# SPDX-License-Identifier: Apache-2.0
