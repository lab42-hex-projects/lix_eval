defmodule LixEval.Evaluator do
  use LixEval.Types

  @moduledoc ~S"""
  Evaluator: Implementation 
  """

  @spec evaluate(LixParser.ast_t, map()) :: result_t()
  def evaluate(ast, env) do
    case ast do
      {:lit, value} -> {:ok, {env, value}}
      {:id, _, _} = id -> _eval_idents(id, env)
      bad -> {:error, "Illegal ast: #{inspect bad}"}
    end
  end

  @spec parse_eval(binary(), map()) :: result_t()
  def parse_eval(source, env \\ %{}) do
    with {:ok,  ast} <- LixParser.parse(source), do: evaluate(ast, env)
  end

  defp _eval_idents({:id, pos, idents}, env) do
    case _lookup(idents, env) do
      {:error, message} -> {:error, "#{message} at #{inspect(pos)}"} 
      val -> {:ok, {env, val}}
    end
  end

  defp _lookup(idents, env) do
    idents 
    |> Enum.reduce_while(env,
      fn k, e ->
        case Map.fetch(e, k) do
          :error -> {:halt, {:error, "Undefined name #{k} (#{idents|>Enum.join(".")})"}}
          {:ok, val} -> {:cont, val}
        end
      end
    )
  end

end
# SPDX-License-Identifier: Apache-2.0
