defmodule LixEval.Types do
  @moduledoc ~S"""
  Types used throughout this appliaction
  """

  defmacro __using__(_opts) do
    quote do
      @type binaries :: list(binary())

      @type either(left, right) :: {:error, left} | {:ok, right}

      @type position_t :: {pos_integer(), pos_integer()}

      @type result_t :: either(binary(), value_t())

      @type value_t :: {map(), any()}
    end
  end
end

# SPDX-License-Identifier: Apache-2.0
