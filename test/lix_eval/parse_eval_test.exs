defmodule Test.LixEval.ParseEvalTest do
  use Support.LixEvalCase

  @env %{
    "a" => 1
  }
  describe "env lookup" do
    test "single name exists" do
      assert parse_eval("a", @env) == {:ok, {@env, 1}} 
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
