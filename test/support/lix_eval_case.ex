defmodule Support.LixEvalCase do
  @moduledoc ~S"""
  Test Support
  """

  defmacro __using__(_opts) do
    quote do
      use ExUnit.Case
      import LixEval
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
